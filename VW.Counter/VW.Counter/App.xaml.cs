﻿using System.Windows;

namespace VW.Counter
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {

            if (e.Args.Length > 2)
            {
                DB_Communication.DBPathString = e.Args[2];
                MainWindow window = new MainWindow(int.Parse(e.Args[0]), int.Parse(e.Args[1]));
                window.Show();
            }

        }
    }
}
