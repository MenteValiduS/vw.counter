﻿using System.Data;
using System.Data.SQLite;
using System.IO;

namespace VW.Counter
{
    class DB_Communication
    {
        private static string m_DBPathString;

        public static string DBPathString { get => m_DBPathString; set => m_DBPathString = value; }

        private static string GetConnectionString()
        {

            if (File.Exists(DBPathString))
            {
                return "URI=file:" + DBPathString;
            }

            return string.Empty;
        }

        public static void Insert(Session session)
        {

            //int gameID = GetGameID(session.game);

            SQLiteConnection connection = new SQLiteConnection(GetConnectionString());
            connection.Open();

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "INSERT INTO Sessions " +
                    "(game, play_time, date, additional)" +
                    "VALUES " +
                    "(@game, @play_time, @date, @additional)";

                cmd.Parameters.Add(new SQLiteParameter("game", session.game));
                cmd.Parameters.Add(new SQLiteParameter("play_time", session.gameTime));
                cmd.Parameters.Add(new SQLiteParameter("date", session.date));
                cmd.Parameters.Add(new SQLiteParameter("additional", session.additionalInfo));

                var result = cmd.ExecuteNonQuery();
            }

            connection.Close();
        }

        public static int GetGameID(string name)
        {
            int ID = 55;

            SQLiteConnection connection = new SQLiteConnection(GetConnectionString());
            connection.Open();

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT ID, name FROM Games";// +
                                                                //"WHERE name=@name";

                cmd.Parameters.Add(new SQLiteParameter("name", name));

                using (var reader = cmd.ExecuteReader())
                {

                    while (reader.Read())
                    {

                        if (reader.GetString(1).Equals(name))
                        {
                            ID = reader.GetInt32(0);
                        }

                    }

                }

            }

            connection.Close();
            return ID;
        }

        /// <summary>
        /// Получение данных игры.
        /// </summary>
        /// <param name="ID">ID игры</param>
        /// <returns></returns>
        public static Game GetGameInfo(int ID)
        {
            Game game = new Game();
            game.ID = ID;

            SQLiteConnection connection = new SQLiteConnection(GetConnectionString());
            connection.Open();

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select name, exe_path " +
                    "from Games WHERE ID=@id";

                cmd.Parameters.Add(new SQLiteParameter("id", game.ID));

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        game.name = reader.GetString(0);
                        game.path = reader.GetString(1);
                    }
                }

            }

            connection.Close();
            return game;
        }
    }
}
