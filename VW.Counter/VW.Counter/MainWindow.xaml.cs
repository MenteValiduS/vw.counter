﻿using System.Windows;

namespace VW.Counter
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow(int appID, int time)
        {
            InitializeComponent();

            this.Topmost = true;

            this.DataContext = new WindowViewModel(this, appID, time);
        }
    }
}
