﻿using System;
using System.Timers;
using System.Windows;
using System.Windows.Input;

namespace VW.Counter
{
    class WindowViewModel : BaseViewModel
    {
        #region Consts

        private const string PauseImagePath = "/Images/Pause_Button.png";
        private const string ContinueImagePath = "/Images/Play_Button.png";

        #region Messages

        /// <summary>
        /// Сообщение о ручной остановке игры в счётчике.
        /// </summary>
        private const string StopMessage = "Игра была остановлена.";
        /// <summary>
        /// Сообщение об успешном завершении игры.
        /// </summary>
        private const string SuccessMessage = "Никаких проблем не возникло.";
        /// <summary>
        /// Сообщение о завершении игры вне счётчика.
        /// </summary>
        private const string ClosedWthoutReasonMessage = "Игра была остановлена беспричинно.";
        /// <summary>
        /// Сообщение о неудаче запуска игры.
        /// </summary>
        private const string StartErrorMessage = "Не удалось запустить игру.";

        #endregion

        #endregion

        #region Private Members

        /// <summary>
        /// Окно, которая контроллирует данная модель-представление.
        /// </summary>
        private Window mWindow;
        /// <summary>
        /// Объект текущей сессии.
        /// </summary>
        private Session mSession;
        /// <summary>
        /// Объект текущей игры.
        /// </summary>
        private Game mGame;
        /// <summary>
        /// Таймер, производящий отсчёт времени.
        /// </summary>
        private Timer timer;
        /// <summary>
        /// Таймер, производящий запуск игры.
        /// </summary>
        private Timer gameLauncherTimer;
        /// <summary>
        /// Таймер, проверяющий, не завершена ли игра вне счётчика.
        /// </summary>
        private Timer failTimer;
        /// <summary>
        /// Счётчик ошибок запуска игры.
        /// </summary>
        private int gameLaunchersFails;

        /// <summary>
        /// Инициализирующий поток.
        /// </summary>
        private System.Threading.Thread dbThread;

        #endregion

        #region Public Properties

        public int Time { get; set; }
        public string LeftBtn_Image { get; set; }

        #endregion

        #region Public Commands

        public ICommand PauseOrContinueCommand { get; set; }
        public ICommand StopCommand { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Стандартный конструктор.
        /// </summary>
        /// <param name="window"></param>
        public WindowViewModel(Window window, int appID, int time)
        {
            PauseOrContinueCommand = new RelayCommand(PauseOrContinue);
            StopCommand = new RelayCommand(Stop);
            gameLaunchersFails = 0;
            LeftBtn_Image = PauseImagePath;
            mWindow = window;
            mGame = new Game
            {
                ID = appID
            };
            mSession = new Session
            {
                gameTime = time,
                date = DateTime.Now.ToShortDateString()
            };
            Time = time;
            StartInitializingThread();
        }


        #endregion


        #region Private Methods

        /// <summary>
        /// Запуск инициализирующего потока.
        /// </summary>
        private void StartInitializingThread()
        {
            dbThread = new System.Threading.Thread(new System.Threading.ThreadStart(Start));
            dbThread.Start();
        }

        /// <summary>
        /// Начало работы.
        /// </summary>
        private void Start()
        {
            GetInfoFromDB();
            LaunchGame();

            SetTimer();
            dbThread.Abort();
        }

        /// <summary>
        /// Получение данных об игре.
        /// </summary>
        private void GetInfoFromDB()
        {
            mGame = DB_Communication.GetGameInfo(mGame.ID);
        }

        /// <summary>
        /// Вывод информации о сессии в базу данных.
        /// </summary>
        private void CommitToDB(string additionalInfo)
        {
            mSession.game = mGame.ID;
            mSession.additionalInfo = additionalInfo;

            DB_Communication.Insert(mSession);
        }

        private void PauseOrContinue()
        {

            if (LeftBtn_Image.Equals(PauseImagePath))
            {
                failTimer.Enabled = false;
                timer.Enabled = false;
                LeftBtn_Image = ContinueImagePath;
            }
            else
            {
                failTimer.Enabled = true;
                timer.Enabled = true;
                LeftBtn_Image = PauseImagePath;
            }

        }

        private void Stop()
        {
            Shutdown(StopMessage);
        }

        #region Timer

        private void SetTimer()
        {
            timer = new Timer(1000);
            timer.Elapsed += OnTimedEvent;
            timer.AutoReset = true;
            timer.Enabled = true;

            gameLauncherTimer = new Timer(1000);
            gameLauncherTimer.Elapsed += OnGameLauncherEvent;
            gameLauncherTimer.AutoReset = true;
            gameLauncherTimer.Enabled = true;

            failTimer = new Timer(100);
            failTimer.Elapsed += OnFailEvent;
            failTimer.AutoReset = true;
            failTimer.Enabled = false;
        }

        private void OnFailEvent(object sender, ElapsedEventArgs e)
        {
            if (!IsGameLaunched())
            {
                Shutdown(ClosedWthoutReasonMessage);
            }
        }

        private void OnGameLauncherEvent(object sender, ElapsedEventArgs e)
        {

            if (IsGameLaunched())
            {
                gameLauncherTimer.Enabled = false;
                failTimer.Enabled = true;
            }
            else
            {

                if (gameLaunchersFails < 10)
                {
                    gameLaunchersFails++;
                    LaunchGame();
                }
                else
                {
                    Shutdown(StartErrorMessage);
                }

            }

        }

        /// <summary>
        /// Отсчёт времени, по истечении которого происходит завершение игры.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {

            if (Time > 0)
            {

                if (IsGameLaunched())
                {
                    Time -= 1;
                }

            }
            else
            {
                Shutdown(SuccessMessage);
            }

        }

        private void Shutdown(string message)
        {
            CommitToDB(message);

            ShutdownTimer();
        }

        /// <summary>
        /// Отключение таймеров и завершение работы приложения.
        /// </summary>
        private void ShutdownTimer()
        {
            timer.Enabled = false;
            failTimer.Enabled = false;
            timer.Dispose();
            failTimer.Dispose();
            gameLauncherTimer.Dispose();
            ShutdownProcess();


            Application.Current.Dispatcher.Invoke(() => Application.Current.Shutdown());
        }

        private void ShutdownProcess()
        {
            string processName = System.IO.Path.GetFileNameWithoutExtension(mGame.path);

            foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcesses())
            {

                if (process.ProcessName.Contains(processName))
                {
                    process.Kill();
                }

            }
        }

        private void LaunchGame()
        {
            System.Diagnostics.Process.Start(mGame.path);
        }

        private bool IsGameLaunched()
        {
            string processName = System.IO.Path.GetFileNameWithoutExtension(mGame.path);

            foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcesses())
            {

                if (process.ProcessName.Contains(processName))
                {
                    return true;
                }

            }

            return false;
        }
        #endregion


        #endregion

    }
}
