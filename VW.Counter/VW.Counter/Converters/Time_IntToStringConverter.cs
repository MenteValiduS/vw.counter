﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace VW.Counter.Converters
{
    public class Time_IntToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var time = (int)value;

            if (time > 60)
            {
                return new StringBuilder((time / 60).ToString() + " мин " + (time % 60).ToString() + " с");
            }
            else
            {
                return new StringBuilder((time % 60).ToString() + " с");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
